.PHONY: all clean 

NAME = demo

CC = g++

CFLAGS = -Wall -Werror -I$(INC_DIR)

SRC_DIR = src/
OBJ_DIR = obj/
INC_DIR = includes/
BIN_DIR = bin/

SOURCES = $(shell ls $(SRC_DIR))
HEADERS = $(addprefix $(INC_DIR), $(shell ls $(INC_DIR)))

OBJS = $(addprefix $(OBJ_DIR), $(SOURCES:.cpp=.o))

DELIM = ========================================== 

all: run

run: $(BIN_DIR)$(NAME)
	@echo running $(NAME)...
	@echo $(DELIM) 
	@./$(BIN_DIR)$(NAME)
	@echo $(DELIM) 

$(BIN_DIR)$(NAME): $(BIN_DIR)test
	@echo -n Building $@ :
	@$(CC) -o $(BIN_DIR)$(NAME) $(filter-out obj/test.o, $(OBJS))
	@echo \ success

$(BIN_DIR)test: $(OBJS)
	@mkdir -p $(BIN_DIR)
	@$(CC) -o $(BIN_DIR)dtest $(filter-out obj/main.o, $(OBJS))
	@echo Running tests...
	@./$(BIN_DIR)/dtest
	@mv $(BIN_DIR)/dtest $(BIN_DIR)test

$(OBJ_DIR)%.o : $(SRC_DIR)%.cpp $(HEADERS)
	@mkdir -p $(OBJ_DIR)
	@echo -n Building $@ :
	@$(CC) $(CFLAGS) -c -o $@ $<
	@echo \ success

clean:
	@rm -rf $(OBJ_DIR) $(NAME) $(BIN_DIR)
