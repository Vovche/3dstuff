#ifndef DUMMY_TEMPLATE_TESTS_H
#define DUMMY_TEMPLATE_TESTS_H

#include <iostream>
#include <functional>

namespace dtest
{
    template <typename T>
    bool simple_compare(T expected, T actual)
    {
        return expected == actual;
    }

    template<typename T>
    void print_test_info(T expected, T actual, bool compare_result)
    {
        std::cout << "\tExpected: " << expected << std::endl; 
        std::cout << "\tActual: " << actual << std::endl; 
        std::cout << "\tResult: ";
        if (!compare_result)
        {
            std:: cout << "Fuck :(" << std::endl;
            exit(-1);
        }
        std:: cout << "OK" << std::endl;
    }

    template <typename T>
    void lrassert(bool invert_flag, T expected, T actual, std::function<bool(T, T)> compare = simple_compare<T>)
    {
        print_test_info(expected, actual, invert_flag ^ compare(actual, expected));
    }

    template <typename T, typename... Args>
    void assert_eq(T expected, Args... args)
    {
        std::cout << "Equal test: " << std::endl; 
        lrassert<T>(false, expected, args...);
    }

    template <typename T, typename... Args>
    void assert_ne(T expected, Args... args)
    {
        std::cout << "Not equal test: " << std::endl; 
        lrassert<T>(true, expected, args...);
    }
}

#endif
