#include "dummy_template_tests.h"

using dtest::assert_eq;
using dtest::assert_ne;

bool cmp(int a, int b)
{
    return a == b;
}


int main ()
{
    assert_eq(4, 4);
    assert_eq(4, 4, cmp);
    assert_ne(4, 5);
    assert_ne(4, 5, cmp);
    return 0;
}
